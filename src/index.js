const { whatsAppWebhookInit } = require('./webhooks/whatsApp');
const telegramWebhookInit = require('./webhooks/telegram');
const logger = require('./tools/logger');


(function() {
  logger.info('Start Telegram bot');
  telegramWebhookInit();
  logger.info('Telegram bot is ok');
  logger.info('Start WhatsApp bot');
  whatsAppWebhookInit();
  logger.info('WhatsApp bot is ok');
})();
