const { Router } = require('express');
const { startServer } = require('../services/express-server');
const { getBotResponse } = require('../services/http');
const {
  sendTextMessage,
  sendTextMessageWithButtons,
  sendTextWithImageMessage,
} = require('../tools/whatsAppMessages');
const sleep = require('../tools/sleep');

const WEBHOOK_PATH = '/whatsApp';
const routes = Router();

// webhook verification
routes.get(WEBHOOK_PATH, (req, res) => {
  return res.send(req.query['hub.challenge']);
});

// webhook message
routes.post(WEBHOOK_PATH, async (req, res) => {
  try {
    const { messages: msgs, metadata } = req.body.entry[0].changes[0].value;
    const { phone_number_id: waId } = metadata;
    const { interactive, from, text } = msgs[0];
    const body = text ? text.body : interactive[interactive.type].id;
    const resBot = await getBotResponse(body, waId, 'whatsApp');
    const { message, messages, multiple } = resBot;

    if (!multiple) {
      const success = await sendTextMessage(message, waId, from);
      return res.json({ success });
    }

    for (const message of messages) {
      if (Array.isArray(message)) {
        const [text, image] = message;
        await sendTextWithImageMessage(text, image, waId, from);
      } else {
        await sendTextMessage(message, waId, from);
      }
      await sleep(1500);
    }

    if (resBot.returnFeedback) {
      await sendTextMessageWithButtons(resBot.feedbackMessage, waId, from);
    }

    res.json({ success: true });
  } catch (error) {
    res.json({ success: false });
  }
});

const whatsAppWebhookInit = () => {
  startServer(routes);
};

module.exports = {
  whatsAppWebhookInit,
};
