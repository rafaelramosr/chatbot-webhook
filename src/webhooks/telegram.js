const { Telegraf } = require('telegraf');
const config = require('config');
const { getBotResponse } = require('../services/http');
const logger = require('../tools/logger');
const sleep = require('../tools/sleep');

const sendTextMessage = (ctx, message) => {
  ctx.reply(message);
};

const sendMessageWithButtons = (ctx, message) => {
  ctx.reply(message, {
    reply_markup: {
      inline_keyboard: [
        [
          { text: 'Sí', callback_data: 'yes' },
          { text: 'No', callback_data: 'not' },
        ],
      ],
    },
  });
};

const sendTextWithImageMessage = (ctx, imageUrl, message) => {
  ctx.replyWithPhoto({ url: imageUrl }, { caption: message });
};

const telegramBot = async (ctx, customMessage = undefined) => {
  try {
    const sendMessage = customMessage ? customMessage : ctx.message.text;
    const res = await getBotResponse(sendMessage, ctx.chat.id, 'telegram');
    const { message, messages, multiple } = res;

    if (!multiple) {
      return sendTextMessage(ctx, message);
    }

    for (const message of messages) {
      if (Array.isArray(message)) {
        const [text, image] = message;
        sendTextWithImageMessage(ctx, image, text);
      } else {
        sendTextMessage(ctx, message);
      }
      await sleep(1000);
    }

    if (res.returnFeedback) sendMessageWithButtons(ctx, res.feedbackMessage);
  } catch (err) {
    logger.error(`telegramBot - ${err.message}`);
  }
};

function telegramWebhookInit() {
  const bot = new Telegraf(config.get('TELEGRAM').TOKEN);
  bot.start(async (ctx) => await telegramBot(ctx, 'help'));
  bot.help(async (ctx) => await telegramBot(ctx, 'help'));
  bot.action('not', async (ctx) => await telegramBot(ctx, 'No'));
  bot.action('yes', async (ctx) => await telegramBot(ctx, 'Sí'));
  bot.on('sticker', (ctx) => ctx.reply('🤨'));
  bot.on('text', async (ctx) => await telegramBot(ctx));
  bot.on(
    [
      'animation',
      'audio',
      'contact',
      'document',
      'photo',
      'video',
      'video_note',
      'voice',
    ],
    async (ctx) => await telegramBot(ctx, '')
  );

  bot.launch().catch((err) => logger.error(`Telegram launch - ${err.message}`));

  // Enable graceful stop
  process.once('SIGINT', () => bot.stop('SIGINT'));
  process.once('SIGTERM', () => bot.stop('SIGTERM'));
}

module.exports = telegramWebhookInit;
