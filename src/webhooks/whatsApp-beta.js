import { nanoid } from 'nanoid';
import config from 'config';
import path from 'path';

import { getBotResponse } from '../services/http.js';
import logger from '../tools/logger.js';
const __dirname = path.resolve();

const sessionIds = new Map();

const sendMessageWithButtons = async (client, numberSender, message) => {
  const buttons = [
    { buttonText: { displayText: 'Sí' } },
    { buttonText: { displayText: 'No' } },
  ];
  await client.sendButtons(numberSender, message, buttons, 'Opcional');
};

const sendTextMessage = (client, numberSender, message) => {
  return new Promise((resolve, reject) => {
    client
      .sendText(numberSender, message)
      .then((result) => {
        resolve(result);
      })
      .catch((err) =>
        reject(`WP sendTextMessage - ${err.message || err.text}`)
      );
  });
};

const sendTextWithImageMessage = (client, numberSender, imageUrl, message) => {
  return new Promise((resolve, reject) => {
    client
      .sendImage(numberSender, imageUrl, nanoid(), message)
      .then((result) => {
        resolve(result);
      })
      .catch((err) =>
        reject(`WP sendTextWithImageMessage - ${err.message || err.text}`)
      );
  });
};

function setSessionAndUser(senderId) {
  try {
    if (!sessionIds.has(senderId)) {
      sessionIds.set(senderId, nanoid());
    }
  } catch (err) {
    logger.error(`setSessionAndUser - ${err.message || err.text}`);
  }
}

async function start(client) {
  client.onMessage(async (userMessage) => {
    setSessionAndUser(userMessage.from);

    try {
      const res = await getBotResponse(
        userMessage.body,
        userMessage.from.split('@')[0],
        'whatsApp'
      );
      const { message, messages, multiple } = res;

      if (!multiple) {
        return await sendTextMessage(client, userMessage.from, message);
      }

      for (const message of messages) {
        if (Array.isArray(message)) {
          const [text, image] = message;
          await sendTextWithImageMessage(client, userMessage.from, image, text);
        } else {
          await sendTextMessage(client, userMessage.from, message);
        }
      }

      if (res.returnFeedback) {
        await sendMessageWithButtons(
          client,
          userMessage.from,
          res.feedbackMessage
        );
      }
    } catch (err) {
      logger.error(err.message || err.text);
    }
  });
}

export default async function whatsAppWebhookInit() {
  return venom
    .create({
      createPathFileToken: true,
      session: config.get('WHATSAPP').SESSION_ID,
      multidevice: true,
      disableSpins: true,
      disableWelcome: true,
      folderNameToken: 'tokens', //folder name when saving tokens
      mkdirFolderToken: __dirname, //folder name when saving tokens
    })
    .then(async (client) => await start(client))
    .catch((err) => logger.error(err.message || err.text));
}
