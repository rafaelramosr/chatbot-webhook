const config = require('config');
const { post } = require('../services/http');
const fetch = require('node-fetch');

const FB_API_URL = 'https://graph.facebook.com/';
const FB_API_VERSION = 'v16.0';

const sendTextMessage = async (message, waId, phoneNumber) => {
  const headers = [{ Authorization: `Bearer ${config.get('META').TOKEN}` }];
  const response = await post(
    `${FB_API_URL}${FB_API_VERSION}/${waId}/messages`,
    {
      messaging_product: 'whatsapp',
      to: phoneNumber,
      type: 'text',
      text: {
        body: message,
        preview_url: false,
      },
    },
    headers
  );

  return Boolean(response.error);
};

const sendTextMessageWithButtons = async (message, waId, phoneNumber) => {
  const headers = [{ Authorization: `Bearer ${config.get('META').TOKEN}` }];
  const response = await post(
    `${FB_API_URL}${FB_API_VERSION}/${waId}/messages`,
    {
      messaging_product: 'whatsapp',
      recipient_type: 'individual',
      to: phoneNumber,
      type: 'interactive',
      interactive: {
        type: 'button',
        body: { text: message },
        action: {
          buttons: [
            {
              type: 'reply',
              reply: { id: 'Sí', title: 'Sí' },
            },
            {
              type: 'reply',
              reply: { id: 'No', title: 'No' },
            },
          ],
        },
      },
    },
    headers
  );

  return Boolean(response.error);
};

async function sendTextWithImageMessage(caption, link, waId, phoneNumber) {
  const headers = [{ Authorization: `Bearer ${config.get('META').TOKEN}` }];
  const response = await post(
    `${FB_API_URL}${FB_API_VERSION}/${waId}/messages`,
    {
      messaging_product: 'whatsapp',
      recipient_type: 'individual',
      to: phoneNumber,
      type: 'image',
      image: { caption, link },
    },
    headers
  );

  return Boolean(response.error);
}

module.exports = {
  sendTextMessage,
  sendTextMessageWithButtons,
  sendTextWithImageMessage,
};
