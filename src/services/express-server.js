const http = require('http');
const express = require('express');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');

const SERVER_PORT = process.env.PORT || 3001;

// config express
function setExpressConfig(app) {
  app.set('port', SERVER_PORT);
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cors());
  app.use(helmet());
  app.use(compression());
  app.use(
    cors({
      allowedHeaders: ['Content-Type', 'Authorization'],
      origin: ['*'],
      methods: ['GET', 'POST'],
    })
  );

  return app;
}

// listening events
const onError = (error) => {
  if (error.syscall !== 'listen') throw error;

  switch (error.code) {
    case 'EACCES':
      console.error(`Port ${SERVER_PORT} requires elevated privileges`);
      return process.exit(1);
    case 'EADDRINUSE':
      console.error(`Port ${SERVER_PORT} is already in use`);
      return process.exit(1);
    default:
      throw error;
  }
};

const onListening = (server) => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`Listening on ${bind}`);
};

// config server
function startServer(routes) {
  const app = setExpressConfig(express());
  app.use('/', routes);

  const server = http.createServer(app);
  server.listen(SERVER_PORT);
  server.on('error', onError);
  server.on('listening', () => onListening(server));
}

module.exports = {
  onError,
  onListening,
  setExpressConfig,
  startServer,
}
