const fetch = require('node-fetch');
const config = require('config');
const logger = require('../tools/logger');

/**
 * Sends a HTTP POST request.
 *
 * @async
 * @param {string} url - The URL to send the request to.
 * @param {Object} body - The request body.
 * @param {Array} [headers=[]] - An array of objects that represent the request headers.
 * @returns {Promise<Object>} - A Promise that resolves with the response body as a JSON object.
 */
async function post(url, body, headers = []) {
  try {
    const response = await fetch(url, {
      method: 'POST',
      headers: createHeader(headers),
      body: JSON.stringify(body),
    });
    return await response.json();
  } catch (err) {
    logger.error(`getBotResponse - ${err.message}`);
  }
}

/**
 * Creates an HTTP header object for a request.
 *
 * @param {Array} headers - An array of objects representing HTTP headers.
 * @returns {Object} - An object containing the HTTP headers.
 */
function createHeader(customHeaders) {
  let headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (!customHeaders.length) return headers;

  customHeaders.forEach((header) => {
    const [headerName, headerValue] = Object.entries(header)[0];
    headers[headerName] = headerValue;
  });

  return headers;
}

async function getBotResponse(Body, WaId, Origin) {
  const API_URL = `${config.get('API').PATH}${
    config.get('API').VERSION
  }`;
  return await post(`${API_URL}/webhook`, {
    Body,
    Origin,
    WaId,
  });
}

module.exports = {
  createHeader,
  getBotResponse,
  post,
}
